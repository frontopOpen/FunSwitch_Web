/*
 * @Author: Su
 * @Date: 2022-04-08 11:21:48
 * @LastEditTime: 2022-04-18 10:41:47
 * @LastEditors: su
 * @Description:
 * @FilePath: \fz_constructionIOC\src\utils\auth.js
 */
const setSess = (sessName, sessValue) => {
  sessionStorage.setItem(sessName, JSON.stringify(sessValue));
};
const getSess = (sessName) => {
  return JSON.parse(sessionStorage.getItem(sessName));
};

const removeSess = (sessName) => {
  sessionStorage.removeItem(sessName);
};

export { setSess, getSess, removeSess };

export function localGet(key) {
  const value = window.localStorage.getItem(key);
  try {
    return JSON.parse(window.localStorage.getItem(key));
  } catch (error) {
    return value;
  }
}

export function localSet(key, value) {
  window.localStorage.setItem(key, String(value));
}

export function localRemove(key) {
  window.localStorage.removeItem(key);
}
