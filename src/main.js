/*
 * @Author: your name
 * @Date: 2023-03-09 16:12:59
 * @LastEditTime: 2023-03-13 11:36:28
 * @LastEditors: yuhan
 * @Description:
 * @FilePath: \apiadmin\src\main.js
 */
import { createApp } from "vue";
import "./config/permission";
import App from "./App.vue";
import store from "@/store";
import router from "./router/index";
import { VueClipboard } from "@soerenmartius/vue3-clipboard";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import "element-plus/theme-chalk/display.css";
import layoutComp from "./layouts/components/export";
import iconPark from "./plugin/icon-park"; // 注册字节跳动图标
import loadI18n from "./plugin/i18n";

const app = createApp(App);
app.use(VueClipboard);
app.use(ElementPlus);
app.use(router);
app.use(store);
layoutComp(app);
iconPark(app);
loadI18n(app);
app.mount("#app");
