import { createRouter, createWebHashHistory } from "vue-router";
import Layout from "@/layouts/index.vue";
export const constantRoutes = [
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/login/index.vue"),
    meta: {
      title: "登录",
    },
    hidden: true,
  },
  {
    path: "/401",
    name: "401",
    component: () => import("@/views/errorPage/401.vue"),
    hidden: true,
  },
  {
    path: "/404",
    name: "404",
    component: () => import("@/views/errorPage/404.vue"),
    hidden: true,
  },
];
// node修改路由起点
export const asyncRoutes = [
  {
    path: "/",
    name: "ProjectAdmin",
    redirect: "/projectAdmin",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/projectAdmin",
        name: "ProjectAdmin",
        component: () => import("../views/projectAdmin/index.vue"),
        meta: {
          title: "项目管理",
          icon: "icon-display",
        },
      },
    ],
  },
  {
    path: "/overviewPage",
    name: "OverviewPage",
    component: Layout,
    children: [
      {
        path: "/overview",
        name: "Overview",
        component: () => import("../views/overview/index.vue"),
        meta: {
          title: "概览",
          icon: "icon-more-app",
        },
      },
    ],
  },
  {
    path: "/apiPage",
    name: "ApiPage",
    component: Layout,
    children: [
      {
        path: "/apiList",
        name: "ApiList",
        component: () => import("../views/apiPage/index.vue"),
        meta: {
          title: "API管理",
          icon: "icon-api",
        },
      },
    ],
  },
  {
    path: "/indexConfigPage",
    name: "IndexConfigPage",
    component: Layout,
    children: [
      {
        path: "/indexConfig",
        name: "IndexConfig",
        component: () => import("../views/indexConfig/index.vue"),
        meta: {
          title: "指标管理",
          icon: "icon-database-setting",
        },
      },
    ],
  },
  {
    path: "/tokenAdminPage",
    name: "TokenAdminPage",
    component: Layout,
    children: [
      {
        path: "/tokenAdmin",
        name: "TokenAdmin",
        component: () => import("../views/tokenAdmin/index.vue"),
        meta: {
          title: "token管理",
          icon: "icon-database-setting",
        },
      },
    ],
  },
  {
    path: "/dataSourcePage",
    name: "DataSourcePage",
    component: Layout,
    children: [
      {
        path: "/dataSource",
        name: "DataSource",
        component: () => import("../views/dataSource/index.vue"),
        meta: {
          title: "数据源管理",
          icon: "icon-database-setting",
        },
      },
    ],
  },
  {
    path: "/websocketConfigPage",
    name: "websocketConfigPage",
    component: Layout,
    children: [
      {
        path: "/websocketConfig",
        name: "websocketConfig",
        component: () => import("../views/websocketConfig/index.vue"),
        meta: {
          title: "websocket输出配置",
          icon: "icon-database-setting",
        },
      },
    ],
  },
  {
    path: "/sqlScriptPage",
    name: "sqlScriptPage",
    component: Layout,
    children: [
      {
        path: "/sqlScript",
        name: "sqlScript",
        component: () => import("../views/sqlScript/index.vue"),
        meta: {
          title: "sql脚本",
          icon: "icon-database-setting",
        },
      },
    ],
  },
  {
    path: "/staticFilePage",
    name: "StaticFilePage",
    component: Layout,
    children: [
      {
        path: "/staticFile",
        name: "StaticFile",
        component: () => import("../views/staticFile/index.vue"),
        meta: {
          title: "全局静态数据",
          icon: "icon-data-file",
        },
      },
    ],
  },
];
// node修改路由终点

const router = createRouter({
  history: createWebHashHistory(),
  routes: [...constantRoutes, ...asyncRoutes],
});

// reset router
export function resetRouter() {
  router.getRoutes().forEach((route) => {
    const { name } = route;
    if (name) {
      router.hasRoute(name) && router.removeRoute(name);
    }
  });
}

export default router;
