/*
 * @Author: your name
 * @Date: 2022-11-14 15:11:40
 * @LastEditTime: 2022-11-14 18:11:23
 * @LastEditors: yuhan
 * @Description:
 * @FilePath: \apiadmin\src\store\modules\customBreadcrumb.js
 */

const state = () => ({
  breadcrumbList: [
    {
      path: '/projectAdmin',
      name: '首页',
      param: {},
    },
  ],
});
const getters = {
  breadcrumbList: (state) => state.breadcrumbList,
};
const mutations = {
  initBreadcrumb(state) {
    state.breadcrumbList = [
      {
        path: '/projectAdmin',
        name: '首页',
        param: {},
      },
    ];
  },
  setBreadcrumb(state, data) {
    state.breadcrumbList.push(data);
  },
  deleteBreadcrumb(state, path) {
    let targetIndex = state.breadcrumbList.findIndex((item) => item.path === path);
    state.breadcrumbList.splice(targetIndex, 1);
  },
};
const actions = {
  initBreadcrumb({ commit }) {
    commit('initBreadcrumb');
  },
  setBreadcrumb({ commit }, data) {
    commit('setBreadcrumb', data);
  },
  deleteBreadcrumb({ commit }, path) {
    commit('deleteBreadcrumb', path);
  },
};
export default { state, getters, mutations, actions };
