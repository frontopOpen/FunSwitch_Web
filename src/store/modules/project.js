/*
 * @Author: your name
 * @Date: 2022-11-14 15:11:40
 * @LastEditTime: 2023-03-23 15:23:00
 * @LastEditors: yuhan
 * @Description: 项目信息
 * @FilePath: \gitee-funswitch\FunSwitch_Web\src\store\modules\project.js
 */

const state = () => ({
  interProject: false, // 是否进入项目，默认false
  projectId: "", // 项目id
  row: {}, // 改项目全量信息
});
const getters = {
  interProject: (state) => state.interProject,
  projectId: (state) => state.projectId,
  row: (state) => state.row,
};
const mutations = {
  initProject(state) {
    state.interProject = false;
    state.projectId = "";
    state.row = "";
  },
  setProjectInter(state, val) {
    state.interProject = val;
  },
  setProject(state, val) {
    state.row = val;
    state.projectId = val.id;
  },
};
const actions = {
  initProject({ commit }) {
    commit("initProject");
  },
  setProjectInter({ commit }, val) {
    commit("setProjectInter", val);
  },
  setProject({ commit }, val) {
    console.log("setProject", val);
    commit("initProject");
    commit("setProject", val);
    commit("setProjectInter", true);
  },
};
export default { state, getters, mutations, actions };
