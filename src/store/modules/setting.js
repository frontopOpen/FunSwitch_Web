import { themeConfig } from "@/config/theme";
import { setting } from "@/config/setting";
import { getLanguage, setLanguage, setSettings, getSettings } from "@/utils/cookies";

const { theme, fixedHead, refresh, collapse, notice, isBreadcrumb, isLogo, tag } = themeConfig;

const { lang } = setting;

const state = {
  isDev: true, // 是否开发环境
  routerView: true, // 是否显示路由
  isDrawerSetting: false, // 是否打开主题设置
  isDrawer: false, // 是否展开移动端菜单
  collapse,
  refresh,
  theme,
  fixedHead,
  notice,
  isBreadcrumb,
  isLogo,
  tag,
  lang: getLanguage() || lang,
};

const getters = {
  isDev: (state) => state.isDev,
  routerView: (state) => state.routerView,
  isDrawer: (state) => state.isDrawer,
  theme: (state) => state.theme,
  isDrawerSetting: (state) => state.isDrawerSetting,
  refresh: (state) => state.refresh,
  fixedHead: (state) => state.fixedHead,
  notice: (state) => state.notice,
  isBreadcrumb: (state) => state.isBreadcrumb,
  isLogo: (state) => state.isLogo,
  tag: (state) => state.tag,
  settings: (state) => state,
  lang: (state) => state.lang,
};

const mutations = {
  SET_IS_DEV: (state, flag) => {
    state.isDev = flag;
  },
  CHANGE_COLLAPSE: (state) => {
    state.collapse = !state.collapse;
  },
  SET_ROUTER_VIEW: (state) => {
    state.routerView = !state.routerView;
  },
  CHANGE_IS_DRAWER: (state, flag) => {
    state.isDrawer = flag;
  },
  SET_THEME: (state, theme) => {
    state.theme = theme;
  },
  CHANGE_SETTING_DRAWER: (state, flag) => {
    state.isDrawerSetting = flag;
  },
  CHANGE_BREADCRUMB: (state, flag) => {
    state.isBreadcrumb = flag;
  },
  CHANGE_TAG: (state, flag) => {
    state.tag = flag;
  },
  SET_SETTING_OPTIONS: (state, options) => {
    setSettings(options.value);
    Object.assign(state, { ...options.value });
  },
  CHANGE_LANGUAGE: (state, lang) => {
    setLanguage(lang);
    state.lang = lang;
  },
};

const actions = {
  /**
   * @description 切换展开收起
   */
  setIsDev: ({ commit }, flag) => {
    commit("SET_IS_DEV", flag);
  },
  /**
   * @description 切换展开收起
   */
  changeCollapse: ({ commit }) => {
    commit("CHANGE_COLLAPSE");
  },
  /**
   * @description 是否刷新路由
   *  @param {boolean} flag true|false
   */
  setRouterView: ({ commit }, flag) => {
    commit("SET_ROUTER_VIEW", flag);
  },
  /**
   * @description 是否展开移动端菜单
   *  @param {boolean} flag true|false
   */
  changeDrawer: ({ commit }, flag) => {
    commit("CHANGE_IS_DRAWER", flag);
  },
  /**
   * @description 设置主题
   * @param {strinng} theme 系统默认：blue|green|red|default
   */
  setTheme: ({ commit }, theme) => {
    commit("SET_THEME", theme);
  },

  /**
   * @description 是否打开主题设置
   * @param {boolean} flag true|false
   */
  setSettingDrawer: ({ commit }, flag) => {
    commit("CHANGE_SETTING_DRAWER", flag);
  },
  /**
   * @description 是否显示面包导航
   * @param {boolean} flag true|false
   */
  setBreadcrumb: ({ commit }, flag) => {
    commit("CHANGE_BREADCRUMB", flag);
  },
  /**
   * @description 是否显示标签
   * @param {boolean} flag true|false
   */
  setTag: ({ commit }, flag) => {
    commit("CHANGE_TAG", flag);
  },

  /**
   * @description 切换语言
   * @param {string} lang 语言 可选值： zh-cn|en
   */
  changeLanguage: ({ commit }, lang) => {
    commit("CHANGE_LANGUAGE", lang);
  },

  /**
   * @description 设置主题配置信息
   * @param {object} options 配置项
   */
  setSettingOptions: ({ commit }, options) => {
    commit("SET_SETTING_OPTIONS", options);
  },
};

export default {
  getters,
  state,
  mutations,
  actions,
};
