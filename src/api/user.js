/*
 * @Author: your name
 * @Date: 2022-10-27 09:16:31
 * @LastEditTime: 2022-10-27 15:42:14
 * @LastEditors: yuhan
 * @Description:
 * @FilePath: \APIAdmin\src\api\user.js
 */
import request from '@/utils/request.js';
import { setting } from '@/config/setting';
const { tokenName } = setting;
export const login = async (data) => {
  return request({
    url: '/user/login',
    method: 'post',
    data,
  });
};

export const getUserInfo = (accessToken) => {
  return request({
    url: '/userInfo',
    method: 'get',
    data: {
      [tokenName]: accessToken,
    },
  });
};

export const logout = () => {
  return request({
    url: '/logout',
    method: 'post',
  });
};

export const register = async () => {
  return request({
    url: '/register',
    method: 'post',
  });
};
