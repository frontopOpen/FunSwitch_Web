/*
 * @Author: your name
 * @Date: 2023-03-10 16:55:11
 * @LastEditTime: 2023-03-23 15:30:20
 * @LastEditors: yuhan
 * @Description:
 * @FilePath: \gitee-funswitch\FunSwitch_Web\src\config\permission.js
 */
/**
 * @author hujiangjun 1217437592@qq.com
 * @description 路由控制
 */
import router from "@/router";
import store from "@/store";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { getPageTitle } from "@/utils/index";
import { setting } from "@/config/setting";
import { getSess } from "@/utils/auth";
import { ElMessage } from "element-plus";
const { authentication, loginInterception, progressBar, routesWhiteList, recordRoute } = setting;
NProgress.configure({
  easing: "ease",
  speed: 500,
  trickleSpeed: 200,
  showSpinner: false,
});
router.beforeEach((to, from, next) => {
  if (progressBar) NProgress.start();
  const permissed = getSess("permissed");
  // 从首页到其他页面且携带query（说明是进入某个项目），设置全局project信息
  // if (from.path === "/projectAdmin" && to.query.id) {
  //   store.dispatch("project/setProject", { ...to.query });
  // }
  if (to.path === "/login") {
    next();
    return;
  }
  if (permissed) {
    // 从某个项目里回到项目列表
    if (to.path === "/projectAdmin" && from.path !== "/login") {
      store.dispatch("project/setProjectInter", false);
    }
    // 如果没有项目id，则跳回项目页面
    if (to.path !== "/projectAdmin" && !store.getters["project/projectId"]) {
      router.push("/projectAdmin");
    }
    next();
    return;
  } else {
    ElMessage.error("没有权限，请登录");
    router.push("/login");
    store.dispatch("project/setProjectInter", false);
  }
  document.title = getPageTitle(to.meta.title);
});
router.afterEach(() => {
  if (progressBar) NProgress.done();
});
