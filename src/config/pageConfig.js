/*
 * @Author: your name
 * @Date: 2022-11-03 15:42:16
 * @LastEditTime: 2023-03-08 11:10:30
 * @LastEditors: yuhan
 * @Description: 页面配置，格式需满足 xxx_config，以适配命令行快速配置页面
 * @FilePath: \apiadmin\src\config\pageconfig.js
 */
const requestTypeOptions = [
  {
    label: 'get',
    value: 'get',
    colorType: 'primary',
  },
  {
    label: 'post',
    value: 'post',
    colorType: 'success',
  },
];
const pageconfig = {
  // 项目管理
  projectAdmin: {
    formItem: {
      projectName: { name: '项目名称', placeholder: '' },
      domian: { name: '域名', placeholder: '' },
      header: { name: '公共头', placeholder: '' },
      tokenKey: { name: 'token', placeholder: '请求第三方token时的tokenkey' },
      codeKey: { name: 'codeKey', placeholder: '请求成功的字段的key，例：code' },
      code: { name: 'code', placeholder: '请求成功的字段的value，例：200' },
    },
  },
  // api管理
  apiPage_config: {
    // 枚举
    _enum: {
      // 类型 0.普通接口1.混合请求接口(叠加)2.混合请求接口(主键),3.SQL接口 4.静态接口
      dataType: [
        {
          key: 0,
          value: '普通接口',
        },
        {
          key: 1,
          value: '混合请求接口(叠加)',
        },
        {
          key: 2,
          value: '混合请求接口(主键)',
        },
        {
          key: 3,
          value: 'SQL接口',
        },
        {
          key: 4,
          value: '静态接口',
        },
        {
          key: 5,
          value: '回调接口',
        },
      ],
      // 黑白名单规则 0放行 1黑名单 2白名单
      rule: [
        {
          key: 0,
          value: '放行',
        },
        {
          key: 1,
          value: '黑名单',
        },
        {
          key: 2,
          value: '白名单',
        },
      ],
    },
    formItem: {
      name: {
        name: 'API名称',
        placeholder: '例: 不动产数据列表',
      },
      type: {
        name: '类型',
        placeholder: '',
      },
      indexId: {
        name: '指标',
        placeholder: '',
      },
      apiPath: {
        name: '请求路径',
        placeholder: '例：index',
      },
      auto: {
        name: '是否鉴权',
        placeholder: '',
      },
      document: {
        name: '文档',
        placeholder: '',
      },
      staticData: {
        name: '静态数据',
        placeholder: '例: {"key":"静态数据"}',
      },
      writable: {
        name: '接口可写',
        placeholder: '',
      },
      status: {
        name: '是否在线',
        placeholder: '',
      },
      primaryKey: {
        name: '主键',
        placeholder: '例: id',
      },
      rule: {
        name: '黑白名单规则',
        placeholder: '',
      },
      list: {
        name: '黑白名单',
        placeholder: '',
      },
    },
  },
  // 指标管理
  indexConfig_config: {
    // 枚举
    _enum: {
      // 数据类型 0.普通类型(值过滤,key转换) 1.对象转集合 2.过滤集合转对象 3.分组
      dataType: [
        {
          key: 0,
          value: '普通类型(值过滤,key转换)',
          remind: [
            {
              title: '原始报文',
              data: [
                {
                  place: '山东省聊城市',
                  companyName: '聊城子公司',
                },
              ],
            },
            {
              title: '转换参数',
              data: {
                place: 'value',
                companyName: 'name',
              },
            },
            {
              title: '结果报文',
              data: [
                {
                  name: '聊城子公司',
                  value: '山东省聊城市',
                },
              ],
            },
          ],
        },
        {
          key: 1,
          value: '对象转集合',
          remind: [
            {
              title: '原始报文',
              data: [
                {
                  id: 1,
                  name: '金陵广场智能厕所',
                  location: '广东省佛山市南海区金兴路金宁文化广场',
                  projectName: '佛山南海瀚洁',
                  enabledTime: '2022-05-17 00:00:00',
                },
              ],
            },
            {
              title: '转换参数',
              data: {
                list: [
                  {
                    key: 'name',
                    value: 'name',
                    join: {
                      xxx: 'aaa',
                    },
                  },
                  {
                    key: 'name',
                    value: 'info',
                  },
                ],
                common: {
                  common: '测试',
                },
              },
            },
            {
              title: '结果报文',
              data: [
                {
                  common: '测试',
                  name: '金陵广场智能厕所',
                  xxx: 'aaa',
                },
                {
                  common: '测试',
                  info: '金陵广场智能厕所',
                },
              ],
            },
          ],
        },
        {
          key: 2,
          value: '分组',
          remind: [
            {
              title: '原始报文',
              data: [
                {
                  age: 26,
                  name: '张三',
                  type: 1,
                },
                {
                  age: 26,
                  name: '李四',
                  type: 2,
                },
                {
                  age: 26,
                  name: '王五',
                  type: 1,
                },
                {
                  age: 26,
                  name: '小四',
                  type: 1,
                },
              ],
            },
            {
              title: '转换参数',
              data: { name: '测试标题', conversion: { name: 'title' } },
            },
            {
              title: '结果报文',
              data: {
                name: '测试标题',
                data: [
                  {
                    title: '张三',
                    age: 26,
                  },
                  {
                    title: '李四',
                    age: 26,
                  },
                  {
                    title: '王五',
                    age: 26,
                  },
                  {
                    title: '小四',
                    age: 26,
                  },
                ],
              },
            },
          ],
        },
        {
          key: 3,
          value: '分组根据key',
          remind: [
            {
              title: '原始报文',
              data: [
                {
                  age: 26,
                  name: '张三',
                  type: 1,
                },
                {
                  age: 26,
                  name: '李四',
                  type: 2,
                },
                {
                  age: 26,
                  name: '王五',
                  type: 1,
                },
                {
                  age: 26,
                  name: '小四',
                  type: 1,
                },
              ],
            },
            {
              title: '转换参数',
              data: { groupingBy: 'type', conversion: {} },
            },
            {
              title: '结果报文',
              data: [
                {
                  name: 1,
                  data: [
                    {
                      name: '张三',
                      type: 1,
                      age: 26,
                    },
                    {
                      name: '王五',
                      type: 1,
                      age: 26,
                    },
                    {
                      name: '小四',
                      type: 1,
                      age: 26,
                    },
                  ],
                },
                {
                  name: 2,
                  data: [
                    {
                      name: '李四',
                      type: 2,
                      age: 26,
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
      requestTypeOptions,
    },
    formItem: {
      name: {
        name: '名称',
        placeholder: '例: 瀚蓝-普通类型-字段拦截',
      },
      requestType: {
        name: '请求方式',
        placeholder: '例: get',
      },
      url: {
        name: '请求路径',
        placeholder: '例: :5050/grandblueV2b/datastatistics/selcompanyinfolist',
      },
      type: {
        name: '类型',
        placeholder: '请选择类型',
      },
      tokenId: {
        name: '使用token',
        placeholder: '请选择token',
      },
      privateHeader: {
        name: '私有头信息',
        placeholder: '例: {"key":"value"}',
      },
      exclude: {
        name: '过滤字段',
        placeholder: '例: [place,companyName]',
      },
      jsonPath: {
        name: 'json_path',
        placeholder: '例: $..result[*]',
      },
      body: {
        name: '请求body',
        placeholder:
          '例：{"id":${id}}；前端请求接口/indexList?id=1,此时body这里设置为{"id":${id}}，而动态参数设置为["id"]',
      },
      param: {
        name: '动态参数',
        placeholder: '例：["id"]',
      },
      jsScript: {
        name: 'JS脚本',
        placeholder: '请填写JS脚本',
      },
      mapKey: {
        name: '转换参数',
        placeholder:
          '例: {"list":[{"key":"name","value":"name","join":{"xxx":"aaa"}}],"common":{"common":"测试"}}',
      },
      mock: {
        name: '模拟参数',
        placeholder: '',
      },
    },
    codeMirror: `/**
* soure: jsonPath处理后的结果
* params: 前端body请求过来的参数
* 格式：function dataCleaning(soure, params) { return soure + params }
* 补充：请勿修改此段注释，按格式在下方写js即可
*/`,
  },
  // token管理
  tokenAdmin_config: {
    _enum: {
      requestTypeOptions,
    },
    formItem: {
      name: {
        name: '名称',
        placeholder: '例: XXtoken',
      },
      prefix: {
        name: '前缀',
        placeholder: '例: Bearer',
      },
      headers: {
        name: '请求头',
        placeholder: '例: {"content-type":"application/json"}',
      },
      url: {
        name: '请求路径',
        placeholder: '例: :5013/api/account/token',
      },
      requestType: {
        name: '请求方式',
        placeholder: '例: post',
      },
      tokenCache: {
        name: '过期时长',
        placeholder: '单位: 分钟，过期时长: 1 永久 0或空不缓存',
      },
      body: {
        name: '请求包',
        placeholder: '例: {"grantType":"password","account":"datav","password":"Datav123456"}',
      },
      jsonPath: {
        name: 'jsonPath',
        placeholder: '例: $..accessToken',
      },
    },
  },
  // 数据源管理
  dataSource_config: {
    // 枚举
    _enum: {
      // 黑白名单规则 0放行 1黑名单 2白名单
      drive: [
        {
          value: 'mysql',
          label: 'mysql',
        },
      ],
    },
    formItem: {
      name: {
        name: '名称',
        placeholder: '例: 数据源xxx',
      },
      userName: {
        name: '用户名',
        placeholder: '例: root',
      },
      passWord: {
        name: '用户密码',
        placeholder: '例: 123456',
      },
      drive: {
        name: '驱动类型',
        placeholder: '例: mysql',
      },
      url: {
        name: '链接字符串',
        placeholder:
          '例:jdbc:mysql://10.0.3.243:3306/funswitch?serverTimezone=UTC&characterEncoding=utf8',
      },
    },
  },
  // websocket输出配置
  websocketConfig_config: {
    _enum: {
      type: [
        {
          label: '接口回调',
          value: 1,
        },
      ],
    },
    formItem: {
      name: {
        name: '名称',
        placeholder: '请填写名称',
      },
      // topicId: {
      //   name: '主题',
      //   placeholder: '',
      // },
      type: {
        name: '类型',
        placeholder: '请选择',
      },
      sourceId: {
        name: '数据来源',
        placeholder: '请选择',
      },
      path: {
        name: '路径',
        placeholder: '例：test',
      },
    },
  },
  // sql脚本
  sqlScript_config: {
    codeMirror: `/**
* 例：select * from project
*/`,
    formItem: {
      name: {
        name: '名称',
        placeholder: '请填写名称',
      },
      sourceId: {
        name: '数据源',
        placeholder: '请选择数据源',
      },
      script: {
        name: 'sql脚本',
        placeholder: '',
      },
    },
  },
};
export default pageconfig;
