/*
 * @Author: your name
 * @Date: 2022-11-03 15:43:57
 * @LastEditTime: 2023-03-08 10:12:34
 * @LastEditors: yuhan
 * @Description: api地址
 * @FilePath: \apiadmin\src\config\api.mjs
 */
export const apiUrl = {
  // 项目管理
  projectAdmin: {
    dataList: `/project/list`, // 获取项目列表
    create: `/project/save`, // 新增项目
    update: `/project/update`, // 修改项目
    delete: `/project/delete`, // 删除项目
  },
  // api管理
  apiList: {
    dataList: `/apiPath/list`, // API接口列表
    dataListFuzzyQuery: `/apiPath/fuzzyQuery`, // 模糊查询API接口列表
    create: `/apiPath/save`, // 新增API
    update: `/apiPath/update`, // 更新API
    delete: `/apiPath/delete`, // 删除API
    offline: `/apiPath/offline`, // 下线API
    online: `/apiPath/online`, // 上线API
    test: `/test/api`, // 测试API
    callBackList: `/apiPath/callBackList`, // 查询回调接口
  },
  // 全局静态资源
  staticFile: {
    // 获取静态数据列表
    getStaticData: `/staticData/list`,
    // 上传文件
    staticDataWarehousing: `/staticData/warehousing`,
    // 下载模板
    downloadTemplate: `/staticData/downloadTemplate`,
    // 导出最新文件
    exportLatelyFile: `/staticData/export`,
  },
  // 指标管理
  indexConfig: {
    dataList: `/indexConfig/list`, // 获取指标管理列表数据
    create: `/indexConfig/save`, // 保存指标
    update: `/indexConfig/update`, // 修改指标
    delete: `/indexConfig/delete`, // 删除指标
    test: `/test/indexTest`, // 指标测试
  },
  // 数据源
  dataSource: {
    dataList: `/datasource/list`, // 数据源列表数据
    create: `/datasource/save`, // 新增数据源
    update: `/datasource/update`, // 更新数据源
    delete: `/datasource/delete`, // 删除数据源
  },
  // token
  tokenAdmin: {
    test: `/test/token`, // 测试token
    dataList: `/token/list`, // token列表
    create: `/token/save`, // 新增token
    update: `/token/update`, // 修改token
    delete: `/token/delete`, // 删除token
  },
  // jsonpath处理
  jsonPath: `/apiPath/jsonPath`,
  // 测试响应速度
  testApiResponeSpeed: `/test/testApi`,
  // websocket输出配置
  websocketConfig: {
    dataList: `/socketConfig/list`,
    create: `/socketConfig/save`,
    update: `/socketConfig/update`,
    delete: `/socketConfig/delete`,
    online: `/socketConfig/online`,
    offline: `/socketConfig/offline`,
  },
  // sql脚本
  sqlScript: {
    dataList: `/sqlScript/list`,
    create: `/sqlScript/save`,
    update: `/sqlScript/update`,
    delete: `/sqlScript/delete`,
    test: `/test/testSql`,
  },
};
export default apiUrl;
