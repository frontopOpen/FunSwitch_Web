/*
 * @Author: your name
 * @Date: 2023-03-09 17:08:42
 * @LastEditTime: 2023-03-09 17:44:25
 * @LastEditors: yuhan
 * @Description:
 * @FilePath: \apiadmin\.eslintrc.cjs
 */
/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
  root: true,
  extends: ["plugin:vue/vue3-essential", "eslint:recommended", "@vue/eslint-config-prettier"],
  parserOptions: {
    ecmaVersion: "latest",
  },
  rules: {
    // 关闭组件命名规则
    "vue/multi-word-component-names": "off",
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
  },
};
