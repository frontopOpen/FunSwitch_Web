<!--
 * @Author: your name
 * @Date: 2022-10-27 09:16:31
 * @LastEditTime: 2023-03-20 11:19:58
 * @LastEditors: yuhan
 * @Description: 
 * @FilePath: \gitee-funswitch\FunSwitch_Web\README.md
-->

<h1 align="center">FunSwitch</h1>

## 简介
funSwitch是[Frontop公司](https://www.efrontop.cn/?sdclkid=AL2s15jR15-pALg&bd_vid=7412135506836561349)旗下开源的一款接口转换工具，快速解决数字孪生数据接入的痛点。如果你正苦恼于双方对接时数据格式存在差异或者想更改部分数据格式以适配你的需求，那么funSwitch会是你的有力助手

## 写在前面

[Gitee](https://gitee.com/frontopOpen)

[后端开源](https://gitee.com/frontopOpen/FunSwitch)

[视频教学地址](https://space.bilibili.com/1524258371/video)

[在线体验地址pro版 4-16到期](http://43.136.71.66/funswitch)

## 交流群

<img width='300' src="./src/assets/communicationGroup.jpg"/>

## 特性

- vue3 + vite + vuex
- Javascript版本
- codemirror在线编辑器支持输入javascript、sql语法等
- echarts展示数据图标
- excel解析sheet

## 准备

- `Node`: 版本建议 >= 16.13.0 [下载链接](https://nodejs.org/zh-cn/download/)
- `Git`: [版本管理工具](https://www.git-scm.com/download)
- `Visual Studio Code`: [最新版本](https://code.visualstudio.com/Download/)
  - [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur) - vue开发必备
  - [Eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)- 脚本代码检查
  - [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - 代码格式化

## 安装使用

- 获取代码

```sh
git clone https://gitee.com/frontopOpen/FunSwitch_Web.git
```

- 安装依赖

```sh
yarn install
```

- 运行

```sh
yarn dev 
部分电脑本地首次启动可能较慢，请耐心等待
```

- 打包

```sh
yarn build
```

- 测试账号

```sh
账号：admin
密码：frontop
```

## 浏览器支持

本地开发推荐使用`Chrome 80+` 浏览器

支持现代浏览器, 不支持 IE
